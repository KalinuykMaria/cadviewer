import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-ui',
  styleUrls: ['./UserInterface.component.scss'],
  templateUrl: './UserInterface.component.html'
})

export class UserInterfaceComponent implements OnInit {

  title = 'app';

  constructor() {
  }

  ngOnInit() {
  }
}
