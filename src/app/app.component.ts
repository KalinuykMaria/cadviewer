import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { element, text } from '@angular/core/src/render3';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import 'zone.js'
import {UserInterfaceComponent} from './UserInterface/UserInterface.component';

declare let THREE: any;
declare let TWEEN: any ;
declare let CustomOrbitControls: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private scene: any;
  private renderer: any;
  private camera: any;
  private controls: any;
  private raycaster: any;
  private gridGroup: any;
  private grid2DGroup: any;
  private grid3DGroup: any;
  private objGroup: any;
  private selectedGridData: any;
  private mouseClicked: any;
  private dynamicMeshData: any = [];
  private dynamicData: any = [];
  private pointer: any;
  private endPoint: any;
  private finalPoint: any;
  private currentMode: any;
  private currentModeFlag: any;
  private pointArray: any = [];
  private objStartPoint: any;
  private grid2DMaterial: any;
  private grid2DSelectedMaterial: any;
  private grid3DMaterial: any;
  private endPointMaterial: any;
  private grid3DSelectedMaterial: any;
  private grid3DGeometry: any;
  private grid3DLineMaterial: any;
  private hintObject: any;
  private gridActive: any;
  
  public _opened: any;

  constructor() {
    this._opened = false
    this.init();
  }
  ngOnInit() {
    
  }
  private init() {

    this.currentMode = 0;
    this.currentModeFlag = false;
    this.objStartPoint = new THREE.Vector3();
    this.gridActive = true;

    let self = this;
    const WIDTH = document.body.offsetWidth;
    const HEIGHT =  document.body.offsetHeight;
    this.mouseClicked = false;

    this.grid3DLineMaterial = new THREE.LineBasicMaterial( { color: 0xffffff } );

    this.camera = new THREE.PerspectiveCamera( 60, WIDTH / HEIGHT, 0.2, 1000 );
    this.camera.aspect = WIDTH / HEIGHT;
    this.camera.position.set(1,20,30);

    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color( 0x222222 );

    const ambient = new THREE.HemisphereLight(0xFFFFFf, 0xffffff, 0.65)
    ambient.position.set( -0.5, 0.75, -1 );
    this.scene.add( ambient );
    const ambient_2 = new THREE.HemisphereLight(0xFFFFFF, 0x0f0e0d, 0.65 );
    ambient_2.position.set( -0.5, 0.75, -1 );
    this.scene.add( ambient_2 );

    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( WIDTH, HEIGHT );
    this.renderer.setClearColor ( 0xffffff, 1 );
    document.body.appendChild( this.renderer.domElement );
    window.addEventListener( 'resize', self.onWindowResize.bind(this), false );    


    this.controls = new THREE.CustomOrbitControls( this.camera, this.renderer.domElement );
    this.controls.screenSpacePanning = false;
    this.controls.minDistance = 1;
    this.controls.maxDistance = 500;

    this.gridGroup = new THREE.Group();
    this.grid2DGroup = new THREE.Group();
    this.grid3DGroup = new THREE.Group();
    this.objGroup = new THREE.Group();
    
    this.scene.add(this.gridGroup);
    this.scene.add(this.grid2DGroup);
    this.scene.add(this.grid3DGroup);
    this.scene.add(this.objGroup);

    this.pointInit();

    this.scene.add( new THREE.GridHelper( 50, 50 ) );
    let geometry = new THREE.SphereGeometry( 0.1, 8, 8 );
    this.grid2DMaterial = new THREE.MeshBasicMaterial( {color: 0xffffff, opacity: 0.5, transparent: true} );
    this.grid3DMaterial = new THREE.MeshBasicMaterial( {color: 0xffffff, opacity: 0.5, transparent: true} );    
    this.grid2DSelectedMaterial = new THREE.MeshBasicMaterial( {color: 0xff0000, opacity: 1, transparent: false} );
    this.grid3DSelectedMaterial = new THREE.MeshBasicMaterial( {color: 0xff0000, opacity: 1, transparent: false} );
    this.endPointMaterial = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
    this.grid3DGeometry = new THREE.SphereGeometry( 0.1, 32, 32 );
    // this.grid3DGeometry


    this.draw2DGridPoint(50, 50, geometry, this.grid2DMaterial);                                         

    this.raycaster = new THREE.Raycaster();
    this.raycaster.linePrecision = 3;
    
    document.addEventListener( 'mousemove', this.MouseMove.bind(this), false );
    document.addEventListener( 'mousedown', this.MouseDown.bind(this), false );
    document.addEventListener( 'mouseup', this.MouseUp.bind(this), false );
    document.addEventListener("keydown", this.KeyPressEvent.bind(this), false );

    this.render();
  }

  private removeGrid() {
    this.pointer.visible = false;
    if (this.hintObject) {
      this.scene.remove(this.hintObject);
      this.hintObject = null;
    }
    this.endPoint.material = this.endPointMaterial;
    this.selectedGridData = null;
    for (var i = this.grid3DGroup.children.length - 1; i >= 0; i--) {
      this.grid3DGroup.remove(this.grid3DGroup.children[i]);
    }
    for (var i = this.gridGroup.children.length - 1; i >= 0; i--) {
      this.gridGroup.remove(this.gridGroup.children[i]);
    }
    this.gridActive = false;
  }

  redrawGrid(objStartPoint: any) {
    this.removeGrid();
    this.gridActive = true;
    this.endPoint.visible = true;
    this.drawGrid(4, 4, 4, 1, objStartPoint);
  }

  private changeMode(mode: any) {
    this.currentMode = mode;
    if (mode === 0) {
    } else if (mode === 1) {
      this.grid2DMaterial.opacity = 0;
      // this.gridGroup.position.copy(this.objStartPoint);
      // this.objGroup.position.copy(this.objStartPoint);
      // this.grid3DGroup.position.copy(this.objStartPoint);
      this.controls.target.set(this.objStartPoint.x, this.objStartPoint.y, this.objStartPoint.z);
      this.controls.update(); 
      this.drawGrid(4, 4, 4, 1, this.objStartPoint);
      this.gridActive = true;
      this.endPoint.visible = true;
      if (this.dynamicData.length > 0 && this.isEqualPoints(this.dynamicData[this.dynamicData.length - 1],this.objStartPoint)) {

      } else {
        this.dynamicData.push( new THREE.Vector3(this.objStartPoint.x, this.objStartPoint.y, this.objStartPoint.z));
      }
      console.log(this.dynamicData);
      this.endPoint.position.copy( new THREE.Vector3(this.objStartPoint.x, this.objStartPoint.y, this.objStartPoint.z));
      this.endPoint.material = this.endPointMaterial;
    } else if (mode === 2) {
      this.removeGrid();
      this.currentMode = 2;
    }
  }

  private pointInit() {
    var geometry = new THREE.SphereGeometry( 0.1, 32, 32 );
    var material = new THREE.MeshBasicMaterial( {color: 0xff0000} );
    this.pointer = new THREE.Mesh( geometry, material );

    var geometry1 = new THREE.SphereGeometry( 0.15, 32, 32 );
    this.endPoint = new THREE.Mesh( geometry1, this.endPointMaterial );
    this.endPoint.objType = "endpoint"

    this.scene.add( this.pointer );
    this.scene.add( this.endPoint );
    this.endPoint.visible = false;
  }

  private KeyPressEvent(event: any) {
    if (event.defaultPrevented) {
      return; // Should do nothing if the default action has been cancelled
    }
    // if (event.key === "Escape" && this.gridActive === false ) {
    //   this.gridActive = true
    //   this.objStartPoint.copy(this.endPoint.position);  
    //   this.grid3DGroup.visible = true;
    //   this.gridGroup.visible = true;
    //   this.changeMode(1);
    // }

    if (event.key === "Escape" && this.dynamicData.length > 1) {
      this.changeMode(2);
    }

    
  }

  private MouseMove(event: any) {
    event.preventDefault();
    if (Math.abs(event.movementX) + Math.abs(event.movementY) > 2) {
      this.mouseClicked = false;
    }
    let mouse = new THREE.Vector2();
    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
   
    this.raycaster.setFromCamera( mouse, this.camera );
    var intersects = null;

    if (this.currentMode === 0) {
      this.resetGrid2DColor();
    } else if (this.currentMode === 1) {
      this.resetGrid3DColor();
    }
   
    if (this.currentMode === 0) {
      intersects = this.raycaster.intersectObjects( this.grid2DGroup.children, true );
    } else if (this.currentMode === 1) {
      intersects = this.raycaster.intersectObjects( this.grid3DGroup.children, true );
    } else if (this.currentMode === 2) {
      let array = [];
      array.push(this.endPoint);
      intersects = this.raycaster.intersectObjects( array, true );
    }

    if ( intersects.length > 0 ) {
      if (this.currentMode === 0) {
        intersects[ 0 ].object.material = this.grid2DSelectedMaterial;
        this.objStartPoint.copy(intersects[ 0 ].object.position);     
        this.currentModeFlag = true;
      } else if (this.currentMode === 1) {
        if (intersects[ 0 ].object.objType === "pov" && this.gridActive === true ) {
          this.pointer.visible = true;
          this.pointer.position.copy( intersects[ 0 ].point );
          intersects[ 0 ].object.material = this.grid3DSelectedMaterial;
          this.selectedGridData = intersects[ 0 ].object;
          if (this.dynamicData.length >= 1 && this.selectedGridData) {
            let previousPosition = this.dynamicData[this.dynamicData.length - 1];
            if (!this.isEqualPoints(previousPosition, this.selectedGridData.position)) {
              this.drawHintCylinder(previousPosition, this.selectedGridData.position, 0.06);
            }
          }
        }

      } else if (this.currentMode === 2) {
        console.log(this.currentMode)
        if (intersects[ 0 ].object.objType === "endpoint" ) {
          console.log("start point")
          intersects[ 0 ].object.material = this.grid2DSelectedMaterial;
          this.objStartPoint.copy(intersects[ 0 ].object.position);     
          this.currentModeFlag = true;
          this.selectedGridData = intersects[ 0 ].object;
        }
      }
    } else {
      if (this.currentMode === 0) {
        this.currentModeFlag = false;
      } else if (this.currentMode === 1) {
        this.pointer.visible = false;
        if (this.hintObject) {
          this.scene.remove(this.hintObject);
          this.hintObject = null;
        }
        this.selectedGridData = null;
      } else if (this.currentMode === 2) {
        this.endPoint.material = this.endPointMaterial;
        this.selectedGridData = null;
      }
    }
  }

  private MouseDown(event: any) {
    event.preventDefault();
    if (this.currentMode === 0) {
      if ( this.currentModeFlag) {
        this.changeMode(1);
      }
    } else if (this.currentMode === 1) {
      if (event.button === 2 && this.objGroup.children.length > 0) {
        // if (this.currentMode !== 2) {
        //   this.changeMode(2);
        // }
        console.log(this.objGroup.children);
        this.objGroup.remove(this.objGroup.children[this.objGroup.children.length - 1]);
        this.dynamicData.splice(-1, 1);
        console.log(this.dynamicData.length, this.objGroup.children.length)
        if (this.objGroup.children.length === 0) {
          this.objStartPoint.copy( new THREE.Vector3(this.dynamicData[0].x, this.dynamicData[0].y, this.dynamicData[0].z) );
          console.log(this.dynamicData);
          this.selectedGridData = null;
          this.endPoint.visible = true;
          this.endPoint.position.copy( new THREE.Vector3(this.objStartPoint.x, this.objStartPoint.y, this.objStartPoint.z) );
          this.finalPoint = null;
          this.redrawGrid(this.endPoint.position)
          return
        } else {
          if (this.dynamicData.length >= 1) this.endPoint.visible = true;
          let previousPosition = this.dynamicData[this.dynamicData.length - 1];
          this.endPoint.position.copy( new THREE.Vector3(previousPosition.x, previousPosition.y, previousPosition.z) );
          this.redrawGrid(this.endPoint.position)
        }
      }
      this.mouseClicked = true;
      this._opened = true;
    } else if (this.currentMode === 2) {
      console.log("mouse click")
      this.mouseClicked = true;
    }
  }

  private MouseUp(event: any) {
    event.preventDefault();
    if (this.currentMode === 0) {
      this.currentModeFlag = false;
    } else if (this.currentMode === 1) {
    //   console.log(event.type)
      if (!this.selectedGridData || !this.mouseClicked) return;
      let currentPosition = new THREE.Vector3(this.selectedGridData.position.x, this.selectedGridData.position.y, this.selectedGridData.position.z);
      if (this.dynamicData.length >= 1) {
        let previousPosition = this.dynamicData[this.dynamicData.length - 1];
        if (!this.isEqualPoints(previousPosition, currentPosition)) {
          this.drawCylinder(previousPosition, currentPosition, 0.06, true);
          this.dynamicData.push(currentPosition);
          console.log(this.dynamicData);
        }
      }
      

      if (this.dynamicData.length >= 2) this.endPoint.visible = true;
      this.endPoint.position.copy( new THREE.Vector3(currentPosition.x, currentPosition.y, currentPosition.z) );

    } else if (this.currentMode === 2) {
      if (this.selectedGridData && this.mouseClicked && this.selectedGridData.objType === "endpoint" ) {
        this.changeMode(1);
      }
    }
  }

  private draw3DGrid(point1: any, point2: any) {
    // grid3DLineMaterial
    var geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3( point1.x, point1.y, point1.z) );
    geometry.vertices.push(new THREE.Vector3( point2.x, point2.y, point2.z) );
    var line = new THREE.Line( geometry, this.grid3DLineMaterial );
    this.gridGroup.add(line);
  }

  private draw3DGridPoint(point: any) {
    // gridGroup
    // grid2DMaterial
    // grid2DSelectedMaterial
    let item = this.grid3DGroup.children.filter((obj: any) => obj.position.x === point.x && obj.position.y === point.y && obj.position.z === point.z)
    if (item.length === 0) {
      let sphere = new THREE.Mesh( this.grid3DGeometry, this.grid3DMaterial );
      sphere.position.set(point.x, point.y, point.z);
      sphere.objType = "pov";
      this.grid3DGroup.add(sphere);
    }
  }

  private resetGrid3DColor() {
    this.grid3DGroup.children.forEach((element :any) => {
      element.material= this.grid3DMaterial;
    });
  }

  private resetGrid2DColor() {
    this.grid2DGroup.children.forEach((element :any) => {
      element.material = this.grid2DMaterial;
    });
  }  

  private isEqualPoints(point1: any, point2: any) {
    return point1.x === point2.x && point1.y === point2.y && point1.z === point2.z 
  }

  private onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  }

  private render() {
    this.camera.updateProjectionMatrix();
    this.controls.update();
    this.renderer.render( this.scene, this.camera );
    requestAnimationFrame( this.render.bind(this) );
  }

  private draw2DGridPoint(xpoint: any, ypoint: any, geometry:any, material: any) {
    xpoint = xpoint / 2;
    ypoint = ypoint / 2;
    for (let x = -xpoint; x < xpoint; x++) {
      for (let y = -ypoint; y < ypoint; y++) {
        var sphere = new THREE.Mesh( geometry, material );
        sphere.position.set(x, 0, y);
        this.grid2DGroup.add(sphere);
      }
    }
  }

  private drawHintCylinder(point1: any, point2: any, size: any) {
      if (this.hintObject) {
        return
      }
      let vstart = new THREE.Vector3();
      vstart.set(point1.x, point1.y, point1.z);
      let vend = new THREE.Vector3();
      vend.set(point2.x, point2.y, point2.z);
      var HALF_PI = Math.PI * .5;
      var distance = vstart.distanceTo(vend);
      var position  = vend.clone().add(vstart).divideScalar(2);
      var material = new THREE.MeshBasicMaterial( { color: 0x0000ff } );
      this.hintObject = new THREE.Mesh( new THREE.CylinderGeometry(size, size, distance, 10, 10, false),material);
      this.scene.add(this.hintObject);
      var orientation = new THREE.Matrix4();//a new orientation matrix to offset pivot
      var offsetRotation = new THREE.Matrix4();//a matrix to fix pivot rotation
      var offsetPosition = new THREE.Matrix4();//a matrix to fix pivot position
      orientation.lookAt(vstart,vend,new THREE.Vector3(0,1,0));//look at destination
      offsetRotation.makeRotationX(HALF_PI);//rotate 90 degs on X
      orientation.multiply(offsetRotation);//combine orientation with rotation transformations
      this.hintObject.geometry.applyMatrix(orientation)
      this.hintObject.position.set(position.x, position.y, position.z);
  }

  private drawCylinder(point1: any, point2: any, size: any, type: any) {

    let vstart = new THREE.Vector3();
    vstart.set(point1.x, point1.y, point1.z);

    let vend = new THREE.Vector3();
    vend.set(point2.x, point2.y, point2.z);

    var HALF_PI = Math.PI * .5;
    var distance = vstart.distanceTo(vend);
    var position  = vend.clone().add(vstart).divideScalar(2);

    var material = new THREE.MeshBasicMaterial( { color: type ? 0xff0000 : 0xffffff } );
    var cylinder = new THREE.CylinderGeometry(size, size, distance, 10, 10, false);

    var orientation = new THREE.Matrix4();//a new orientation matrix to offset pivot
    var offsetRotation = new THREE.Matrix4();//a matrix to fix pivot rotation
    var offsetPosition = new THREE.Matrix4();//a matrix to fix pivot position
    orientation.lookAt(vstart,vend,new THREE.Vector3(0,1,0));//look at destination
    offsetRotation.makeRotationX(HALF_PI);//rotate 90 degs on X
    orientation.multiply(offsetRotation);//combine orientation with rotation transformations
    cylinder.applyMatrix(orientation)

    var mesh = new THREE.Mesh(cylinder,material);
    mesh.position.set(position.x, position.y, position.z);
    mesh.gridData = {
      type,
      point1,
      point2,
    };
    this.objGroup.add(mesh);
  }

  private mapTo3DPos(xSize: any, ySize: any, zSize: any, scale: any) {
    let points = [];
    for (let x = 0; x < xSize; x++) {
      for (let y = 0; y < ySize; y++) {
        for (let z = 0; z < zSize; z++) {
          if (x - 1 > 0) points.push([[x, y, z], [x - 1, y, z]]);
          if (x + 1 < xSize) points.push([[x, y, z], [x + 1, y, z]]);
          if (y - 1 > 0) points.push([[x, y, z], [x, y - 1, z]]);
          if (y + 1 < ySize) points.push([[x, y, z], [x, y + 1, z]]);
          if (z - 1 > 0) points.push([[x, y, z], [x, y, z - 1]]);
          if (z + 1 < zSize) points.push([[x, y, z], [x, y, z + 1]]);
        }
      }
    }
    let finals : any = [];
    points.forEach(point => {
      let prev = point[0];
      let next = point[1];
      let value = finals.filter((final: any) => {
        return  ((final[0][0] === prev[0] && final[0][1] === prev[1] && final[0][2] === prev[2] &&
             final[1][0] === next[0] && final[1][1] === next[1] && final[1][2] === next[2])) ||
             ((final[0][0] === next[0] && final[0][1] === next[1] && final[0][2] === next[2] &&
              final[1][0] === prev[0] && final[1][1] === prev[1] && final[1][2] === prev[2]))
      })
      if (value.length === 0) finals.push(point)
    })
    for (let i = 0; i < finals.length; i++) {
      finals[i][0][0] = finals[i][0][0] * scale - (xSize - 1) / 2 * scale
      finals[i][0][1] = finals[i][0][1] * scale - (ySize - 1) / 2 * scale
      finals[i][0][2] = finals[i][0][2] * scale - (zSize - 1) / 2 * scale
      finals[i][1][0] = finals[i][1][0] * scale - (xSize - 1) / 2 * scale
      finals[i][1][1] = finals[i][1][1] * scale - (ySize - 1) / 2 * scale
      finals[i][1][2] = finals[i][1][2] * scale - (zSize - 1) / 2 * scale
    }
    return finals;
  }

  private drawGrid (xSize: any, ySize: any, zSize: any, scale: any, objStartPoint: any) {
    let data = this.mapTo3DPos(xSize + 1, ySize + 1, zSize + 1, scale);
    data.forEach((element : any) => {
      let previousPosition = new THREE.Vector3();
      let currentPosition = new THREE.Vector3();

      let x = objStartPoint.x;
      let y = objStartPoint.y;
      let z = objStartPoint.z;

      previousPosition.set(element[0][0] + x, element[0][1] + y, element[0][2] + z)
      currentPosition.set(element[1][0] + x, element[1][1] + y, element[1][2] + z)
      this.draw3DGrid(previousPosition, currentPosition);
      this.draw3DGridPoint(previousPosition);
      this.draw3DGridPoint(currentPosition);
    })
  }
}
