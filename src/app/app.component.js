"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("zone.js");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.dynamicMeshData = [];
        this.dynamicData = [];
        this._opened = false;
        this.init();
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent.prototype.init = function () {
        var self = this;
        var WIDTH = document.body.offsetWidth;
        var HEIGHT = document.body.offsetHeight;
        this.mouseClicked = false;
        this.camera = new THREE.PerspectiveCamera(60, WIDTH / HEIGHT, 0.2, 1000);
        this.camera.aspect = WIDTH / HEIGHT;
        this.camera.position.set(1, 20, 30);
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0x222222);
        var ambient = new THREE.HemisphereLight(0xFFFFFf, 0xffffff, 0.65);
        ambient.position.set(-0.5, 0.75, -1);
        this.scene.add(ambient);
        var ambient_2 = new THREE.HemisphereLight(0xFFFFFF, 0x0f0e0d, 0.65);
        ambient_2.position.set(-0.5, 0.75, -1);
        this.scene.add(ambient_2);
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(WIDTH, HEIGHT);
        this.renderer.setClearColor(0xffffff, 1);
        document.body.appendChild(this.renderer.domElement);
        window.addEventListener('resize', self.onWindowResize.bind(this), false);
        this.scene.add(new THREE.GridHelper(150, 150));
        this.controls = new THREE.CustomOrbitControls(this.camera, this.renderer.domElement);
        this.controls.screenSpacePanning = false;
        this.controls.minDistance = 1;
        this.controls.maxDistance = 500;
        this.gridGroup = new THREE.Group();
        this.objGroup = new THREE.Group();
        this.scene.add(this.gridGroup);
        this.scene.add(this.objGroup);
        this.drawGrid(4, 4, 4, 1.5);
        this.pointInit();
        this.raycaster = new THREE.Raycaster();
        this.raycaster.linePrecision = 3;
        document.addEventListener('mousemove', this.MouseMove.bind(this), false);
        document.addEventListener('mousedown', this.MouseDown.bind(this), false);
        document.addEventListener('mouseup', this.MouseUp.bind(this), false);
        this.render();
    };
    AppComponent.prototype.pointInit = function () {
        var geometry = new THREE.SphereGeometry(0.1, 32, 32);
        var material = new THREE.MeshBasicMaterial({ color: 0xff0000 });
        this.pointer = new THREE.Mesh(geometry, material);
        var geometry1 = new THREE.SphereGeometry(0.1, 32, 32);
        var material1 = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        this.endPoint = new THREE.Mesh(geometry1, material1);
        this.scene.add(this.pointer);
        this.scene.add(this.endPoint);
        this.endPoint.visible = false;
    };
    AppComponent.prototype.MouseMove = function (event) {
        event.preventDefault();
        this.mouseClicked = false;
        var mouse = new THREE.Vector2();
        mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
        this.resetGridColor();
        this.raycaster.setFromCamera(mouse, this.camera);
        var intersects = this.raycaster.intersectObjects(this.gridGroup.children, true);
        if (intersects.length > 0) {
            if (intersects[0].object.gridData.type)
                return;
            this.pointer.visible = true;
            this.pointer.position.copy(intersects[0].point);
            intersects[0].object.material.color.setHex(0xff0000);
            this.selectedGridData = intersects[0].object.gridData;
        }
        else {
            this.pointer.visible = false;
            this.selectedGridData = null;
        }
    };
    AppComponent.prototype.MouseDown = function (event) {
        event.preventDefault();
        if (event.button === 2 && this.objGroup.children.length !== 0) {
            this.objGroup.remove(this.objGroup.children[this.objGroup.children.length - 1]);
            var lastData = this.dynamicData[this.dynamicData.length - 1];
            this.dynamicData.splice(-1, 1);
            if (this.dynamicData.length === 0) {
                this.selectedGridData = null;
                this.endPoint.visible = false;
                this.finalPoint = null;
                return;
            }
            else {
                this.selectedGridData = this.dynamicData[this.dynamicData.length - 1];
                var previousPosition = this.selectedGridData.point1;
                var currentPosition = this.selectedGridData.point2;
                if (this.dynamicData.length > 1)
                    this.endPoint.visible = true;
                if (this.isEqualPoints(lastData.point1, currentPosition) || this.isEqualPoints(lastData.point2, currentPosition)) {
                    this.endPoint.position.copy(new THREE.Vector3(currentPosition.x, currentPosition.y, currentPosition.z));
                    this.finalPoint = currentPosition;
                }
                else if (this.isEqualPoints(lastData.point1, previousPosition) || this.isEqualPoints(lastData.point2, previousPosition)) {
                    this.endPoint.position.copy(new THREE.Vector3(previousPosition.x, previousPosition.y, previousPosition.z));
                    this.finalPoint = previousPosition;
                }
                else {
                    return;
                }
            }
        }
        this.mouseClicked = true;
        this._opened = true;
    };
    AppComponent.prototype.MouseUp = function (event) {
        var _this = this;
        event.preventDefault();
        console.log(event.type);
        if (!this.selectedGridData || !this.mouseClicked)
            return;
        var previousPosition = this.selectedGridData.point1;
        var currentPosition = this.selectedGridData.point2;
        var count = this.dynamicData.filter(function (data) { return _this.isEqualPoints(data.point1, previousPosition) && _this.isEqualPoints(data.point2, currentPosition); }).length;
        if (count !== 0)
            return;
        if (this.dynamicData.length !== 0) {
            var lastData = this.dynamicData[this.dynamicData.length - 1];
            if (this.finalPoint && !this.isEqualPoints(currentPosition, this.finalPoint) && !this.isEqualPoints(previousPosition, this.finalPoint)) {
                return;
            }
            else if (this.isEqualPoints(lastData.point1, currentPosition) || this.isEqualPoints(lastData.point2, currentPosition)) {
                this.endPoint.position.copy(new THREE.Vector3(previousPosition.x, previousPosition.y, previousPosition.z));
                this.finalPoint = previousPosition;
                this.endPoint.visible = true;
            }
            else if (this.isEqualPoints(lastData.point1, previousPosition) || this.isEqualPoints(lastData.point2, previousPosition)) {
                this.endPoint.position.copy(new THREE.Vector3(currentPosition.x, currentPosition.y, currentPosition.z));
                this.finalPoint = currentPosition;
                this.endPoint.visible = true;
            }
            else {
                return;
            }
        }
        this.dynamicData.push({
            point1: previousPosition,
            point2: currentPosition,
        });
        this.drawCylinder(previousPosition, currentPosition, 0.06, true);
    };
    AppComponent.prototype.resetGridColor = function () {
        this.gridGroup.children.forEach(function (element) {
            element.material.color.setHex(0xffffff);
        });
    };
    AppComponent.prototype.isEqualPoints = function (point1, point2) {
        return point1.x === point2.x && point1.y === point2.y && point1.z === point2.z;
    };
    AppComponent.prototype.onWindowResize = function () {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
    };
    AppComponent.prototype.render = function () {
        this.camera.updateProjectionMatrix();
        this.controls.update();
        this.renderer.render(this.scene, this.camera);
        requestAnimationFrame(this.render.bind(this));
    };
    AppComponent.prototype.drawCylinder = function (point1, point2, size, type) {
        var vstart = new THREE.Vector3();
        vstart.set(point1.x, point1.y, point1.z);
        var vend = new THREE.Vector3();
        vend.set(point2.x, point2.y, point2.z);
        var HALF_PI = Math.PI * .5;
        var distance = vstart.distanceTo(vend);
        var position = vend.clone().add(vstart).divideScalar(2);
        var material = new THREE.MeshBasicMaterial({ color: type ? 0xff0000 : 0xffffff });
        var cylinder = new THREE.CylinderGeometry(size, size, distance, 10, 10, false);
        var orientation = new THREE.Matrix4(); //a new orientation matrix to offset pivot
        var offsetRotation = new THREE.Matrix4(); //a matrix to fix pivot rotation
        var offsetPosition = new THREE.Matrix4(); //a matrix to fix pivot position
        orientation.lookAt(vstart, vend, new THREE.Vector3(0, 1, 0)); //look at destination
        offsetRotation.makeRotationX(HALF_PI); //rotate 90 degs on X
        orientation.multiply(offsetRotation); //combine orientation with rotation transformations
        cylinder.applyMatrix(orientation);
        var mesh = new THREE.Mesh(cylinder, material);
        mesh.position.set(position.x, position.y, position.z);
        mesh.gridData = {
            type: type,
            point1: point1,
            point2: point2,
        };
        if (type) {
            this.objGroup.add(mesh);
        }
        else {
            this.gridGroup.add(mesh);
        }
        // this.dynamicMeshData.push({
        //   type,
        //   mesh
        // })
    };
    AppComponent.prototype.mapTo3DPos = function (xSize, ySize, zSize, scale) {
        var points = [];
        for (var x = 0; x < xSize; x++) {
            for (var y = 0; y < ySize; y++) {
                for (var z = 0; z < zSize; z++) {
                    if (x - 1 > 0)
                        points.push([[x, y, z], [x - 1, y, z]]);
                    if (x + 1 < xSize)
                        points.push([[x, y, z], [x + 1, y, z]]);
                    if (y - 1 > 0)
                        points.push([[x, y, z], [x, y - 1, z]]);
                    if (y + 1 < ySize)
                        points.push([[x, y, z], [x, y + 1, z]]);
                    if (z - 1 > 0)
                        points.push([[x, y, z], [x, y, z - 1]]);
                    if (z + 1 < zSize)
                        points.push([[x, y, z], [x, y, z + 1]]);
                }
            }
        }
        var finals = [];
        points.forEach(function (point) {
            var prev = point[0];
            var next = point[1];
            var value = finals.filter(function (final) {
                return ((final[0][0] === prev[0] && final[0][1] === prev[1] && final[0][2] === prev[2] &&
                    final[1][0] === next[0] && final[1][1] === next[1] && final[1][2] === next[2])) ||
                    ((final[0][0] === next[0] && final[0][1] === next[1] && final[0][2] === next[2] &&
                        final[1][0] === prev[0] && final[1][1] === prev[1] && final[1][2] === prev[2]));
            });
            if (value.length === 0)
                finals.push(point);
        });
        for (var i = 0; i < finals.length; i++) {
            finals[i][0][0] = finals[i][0][0] * scale - (xSize - 1) / 2 * scale;
            finals[i][0][1] = finals[i][0][1] * scale - (ySize - 1) / 2 * scale;
            finals[i][0][2] = finals[i][0][2] * scale - (zSize - 1) / 2 * scale;
            finals[i][1][0] = finals[i][1][0] * scale - (xSize - 1) / 2 * scale;
            finals[i][1][1] = finals[i][1][1] * scale - (ySize - 1) / 2 * scale;
            finals[i][1][2] = finals[i][1][2] * scale - (zSize - 1) / 2 * scale;
        }
        return finals;
    };
    AppComponent.prototype.drawGrid = function (xSize, ySize, zSize, scale) {
        var _this = this;
        var data = this.mapTo3DPos(xSize + 1, ySize + 1, zSize + 1, scale);
        data.forEach(function (element) {
            var previousPosition = new THREE.Vector3();
            var currentPosition = new THREE.Vector3();
            previousPosition.set(element[0][0], element[0][1], element[0][2]);
            currentPosition.set(element[1][0], element[1][1], element[1][2]);
            _this.drawCylinder(previousPosition, currentPosition, 0.025, false);
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map